# Ground Rules

0. Never edit contents of `index.php`. If you think you need to add some code here to complete integration work, please contact a Coredna Developer for a better solution. 
0. Never create an `index.html` file in `/templates` folder. If you think you need this file to complete integration work, please contact a Coredna Developer for an alternate solution for your requirement. 
0. Do not add zip files to this repository.
0. Do not add content images in this repository. Images related to content should be uploaded to Assets module in Coredna DXP. However you may keep essential layout related images in `public/images` folder such as logos, backgrounds etc.

# Installation Instructions

## Requirements - Node

0. Clone repository.      
0. Run `npm install` or `yarn` from the command line.     
0. Run `npm run dev` to start development.      
0. Run `npm run build` to compile Stylesheets and JavaScript for production.     








