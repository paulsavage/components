
# coredna-style-guide-webpack

[Webpack](https://webpack.js.org/) decorators for integrating with the [Coredna Style Guide](https://bitbucket.org/coredna/components/src/master/coredna-style-guide/).


## Setup

First, decorate your server Webpack config:

```js
const decorateServerConfig = require('coredna-style-guide-webpack').decorateServerConfig;

module.exports = decorateServerConfig({
  // Webpack config...
});
```

Then, decorate your client Webpack config:

```js
const decorateClientConfig = require('seek-style-guide-webpack').decorateClientConfig;
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCss = new ExtractTextPlugin({
  filename: 'style.css'
});

const config = {
  // Webpack config...
};

module.exports = decorateClientConfig(config, {
  // Ensure you pass your ExtractTextPlugin instance to the decorator, if required:
  extractTextPlugin: extractCss
});
```

Please note that, if your Webpack loaders aren't scoped to your local project files via the ["include" option](https://webpack.github.io/docs/configuration.html#module-loaders), the decorator will throw an error.

### Extra includes

If you have other external node_modules that need to be compiled in the same way as the seek-style-guide then you can pass an extra parameter to the decorators.

```js
module.exports = decorateClientConfig(config, {
  // List of node_modules that need to be compiled by webpack
  extraIncludePaths: ['my-other-module']
});
```

### CSS Selector Prefix

This selector prefix is automatically prepended to all selectors to ensure styles don't leak out into the global scope.
For example, this is used for generating the standalone header & footer in the style guide.

```js
module.exports = decorateClientConfig(config, {
  cssSelectorPrefix: '.__SSG__'
});
```

## License

MIT.
