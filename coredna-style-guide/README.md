
# coredna-style-guide

Living style guide for [Coredna](https:www.coredna.com), powered by [React](https://facebook.github.io/react), [webpack](https://webpack.js.org), [CSS Modules](https://github.com/css-modules/css-modules) and [SCSS](https://sass-lang.com).

## Installation

Download and copy this styleguide in your project.

## Setup

Wrap your app with the `StyleGuideProvider` component to use any of the style guide components. For example:

```js
import React, { Component } from 'react';
import { StyleGuideProvider } from 'coredna-style-guide/react';

export default class App extends Component {
  render() {
    const locale = 'AU';
    const title = '...';
    const meta = [
      { name: 'description', content: '...' }
    ];
    const link = [
      { rel: 'canonical', href: 'https://www.coredna.com' }
    ];

    return (
      <StyleGuideProvider locale={locale} title={title} meta={meta} link={link}>
        ...
      </StyleGuideProvider>
    );
  }
};
```

`StyleGuideProvider`'s props are used to set the page head properties using [Helmet](https://github.com/nfl/react-helmet).


## High Level Components

As much as possible, you should aim to minimise the amount of custom CSS you need to write. This is achieved by leveraging our suite of high level components. You can run `npm start` to see a demo of components. 


### Responsive Breakpoint

The style guide exposes one responsive breakpoint:

```scss
$responsive-breakpoint: 740px;
```

Content should otherwise be responsive within its container. The set of included components follow this model internally if you'd like to get a sense of what this looks like in practice.

### Color Variables

As much as possible, colors should be directly imported from the style guide.

The following colors are provided:

```scss
// Brand colors
$dna-blue
$dna-pink
$dna-green
$dna-purple
$dna-teal

// Partner brand colors
$dna-business
$dna-volunteer
$dna-learning-light
$dna-learning-medium
$dna-learning-dark

// Grays
$dna-black
$dna-charcoal
$dna-mid-gray-dark
$dna-mid-gray-medium
$dna-mid-gray
$dna-mid-gray-light
$dna-gray-light
$dna-gray-lightest
$dna-off-white
$dna-white

// Element colors
$dna-link
$dna-link-visited
$dna-focus
$dna-highlight
$dna-green-light
$dna-yellow
$dna-yellow-light
$dna-orange
$dna-footer
$dna-background
$dna-yellow
```

### Z-Indexes

To ensure correct relative elements stacking order, z-index variables are provided:

```scss
$z-index-header-overlay
$z-index-header
$z-index-page-overlay
$z-index-inline-overlay
$z-index-negative
```

### Accessible Color Variants

The contrast ratio of certain foreground/background color combinations don't meet the [AA accessibility standards](https://www.w3.org/WAI/WCAG20/quickref/#qr-visual-audio-contrast-contrast) that we aim for. As a result, a suite of accessible variants have been provided:

```scss
$dna-mid-gray-on-white
$dna-pink-on-gray-light
$dna-learning-dark-on-gray-light
$dna-business-on-gray-light
$dna-link-on-mid-gray-light
$dna-mid-gray-dark-on-gray-light
```

Please note that this list is not exhaustive, so contributions are encouraged. To validate color combinations, we recommend the use of the web-based tool [Accessible Colors](http://accessible-colors.com) by [@moroshko](https://github.com/moroshko).

### Grid Variables

In order to ensure elements correctly follow the grid, element sizing should always be controlled by the following variables:

```scss
$grid-row-height
$grid-gutter-width
$grid-column-width
$grid-container-width
```

When defining a document content container:

```scss
.element {
  max-width: $grid-container-width;
}
```

When defining heights and vertical padding/margins:

```scss
.element {
  height: ($grid-row-height * 3);
  padding-bottom: $grid-row-height;
  margin-bottom: $grid-row-height;
}
```

When defining widths and horizontal padding/margins:

```scss
.element {
  width: ($grid-column-width * 3);
  padding-right: $grid-gutter-width;
  margin-right: $grid-column-width;
}
```

It's important to note that any additions to these values (e.g. borders) will need to be negated to maintain rhythm:

```scss
.element {
  $border-width: 1px;
  border-bottom: $border-width solid $dna-charcoal;
  padding-bottom: $grid-row-height - $border-width;
}
```

## License

MIT.
