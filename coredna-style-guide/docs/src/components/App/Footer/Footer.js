import styles from './Footer.scss';
import React from 'react';
import { PageBlock, Section } from 'seek-style-guide/react';
import GithubIcon from './GithubIcon/GithubIcon';

export default function Footer() {
  return (
    <PageBlock>
      <Section>
        <div className={styles.root}>
          <a
            className={styles.link}
            href="https://bitbucket.org/coredna/components/src/master/coredna-style-guide/"
            title="View project on BitBucket">
            View project on BitBucket
          </a>
        </div>
      </Section>
    </PageBlock>
  );
}
