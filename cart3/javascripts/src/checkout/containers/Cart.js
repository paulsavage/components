import $ from 'jquery'
import {updateContinueBtn, isEmail} from "../global/index"
import store from '../reducers'

const CartApp = (() => {

    let quantity = '.quantity'
    let $removeProduct = $('.product-remove')
    let product = 'form'
    let cart3Store;

    const insertQuantityTextField = function(quantity, $product) {
        let html = `<input class="product__quantity__text quantity" data-quantity="${quantity}"  
            pattern="[0-9]*" type="text" value="${quantity}" />`
        $product.find('.quantity__fields').html(html)
        $product.find('.quantity').focus().select()
    }

    const updateProduct = function (quantity, $product) {

        let productID = $product.data('productid');
        let variantID = $product.data('variantid');
        //let productPrice = $product.find('.content__product .product__total .total--desktop').text();
        //let productName = $product.find('.content__product .product__description h3').text();
        let $quantityDropdown = $product.find('.product__quantity');
        let $quantityTextbox = $product.find('.product__quantity__text');
        let currentQuantity = $quantityDropdown.length ? $quantityDropdown.val() : $quantityTextbox.val();

        $.ajax({
            url: '/index.php?action=prodcataloguecart&json=1&form_name=cart3&form_action=update_product',
            type: 'post',
            dataType: 'json',
            data: {
                product_id: productID,
                quantity: quantity,
                variant_id: variantID
            },
            success: function(data) {

                if(data.success == 'Product Removed from Cart') {

                    // TODO: add code for analytics

                    $product.remove();

                    // Update Cart Total and Items count
                    let totalAmount = data.total.toFixed(2);
                    let itemsCount = $('#cart-items-wrap .content__cart__item').length;

                    $('.content__subtotal .product__total p').html('$' + totalAmount);
                    //$('.content__subtotal .product__actions').html( itemsCount + ' Items');
                    $('#subtotal-mobile').html( totalAmount );
                    //$('#cart-count-mobile').html( itemsCount );

                    // disable continue button if cart is empty now
                    if ( $('.form__submit').length && $('#cart-items-wrap .content__cart__item').length == 0 ) {
                        $('.form__submit').prop('disabled', true);
                        $('.form__submit').addClass('disabled');
                        $('#empty-cart-message').addClass('active');
                    }

                }
                else if(data.success == 'Product Updated') {

                    /**
                     * Product quantity has been changed so update the totals amount in view
                     */

                    let itemPrice = $product.find('.current__price').data('itemprice');

                    // Update Cart Total
                    let totalAmount = data.subtotal.toFixed(2);

                    $product.find('.product__total p.total--desktop').html('$' + (quantity * itemPrice).toFixed(2));
                    $('.content__subtotal .product__total p').html('$' + totalAmount);
                    $('#subtotal-mobile').html(totalAmount);
                    $('.coredna-count').html(data.itemsCount + ' Items');

                    // Here we just update the quantity in the select dropdown or textfield (whichever exists)
                    // This is needed because if the user typed quantity more than the stock, It updates the quantity accordingly

                    if ( $quantityDropdown.length ) {

                        $quantityDropdown.val(quantity);
                        $quantityDropdown.data('quantity', quantity);

                    }

                    if ( $quantityTextbox.length ) {

                        $quantityTextbox.val(quantity);
                        $quantityTextbox.data('quantity', quantity);

                    }

                }
                else if(data.success.indexOf('stock limit of') > -1) {

                    /**
                     * User entered quantity greater than stock level
                     * Make another AJAX call to update quantity equal to stock
                     */
                    let matches = /stock limit of [0-9]+./.exec(data.success);

                    let stock = /[0-9]+/.exec(matches[0])[0];

                    // show a message to the user
                    let infoHTML = '<h3>Stock Availability</h3>'+
                        '<p>Currently there are ' + stock + ' items in stock for the selected product. ' +
                        'Quantity has been automatically updated to ' + stock + '.</p>';
                    $('#cart-info-msg').html(infoHTML);
                    $('.cta__wrapper').show();
                    $('#modal-close').hide();
                    $('body').addClass('show-modal');

                    updateProduct(stock, $product);

                }
                else if (data.errors.length) {
                    alert('something went wrong. Please try again.');
                }


            },
            error: function (err) {
                console.error(err);
            },
            complete: function () {
                //updateCartCount();
            }
        }).done(function( msg ) {
        });

    }


    const events = () => {



        $('body').on('input', quantity , (event) => {
            event.preventDefault()
            let value = event.currentTarget.value
            let $self = $(event.currentTarget)
            let $product = $self.closest(product);
            if (value === 'more') {
                insertQuantityTextField($self.data('quantity'), $product);
            }
            else {
                updateProduct(value, $product);
            }
        });

        $removeProduct.on('click', function (event) {
            event.preventDefault();
            let $product = $(this).closest(product);
            updateProduct(0, $product);
        });

        $('#email').on('input change', function (e) {
            if (isEmail($(this).val())) {
                $(this).siblings('.input__info').text('');
                $(this).closest('.form__item').removeClass('form__item--error');
                // enable continue button
                updateContinueBtn(true)
            }
            else {
                $(this).siblings('.input__info').text('Please enter a valid email address');
                $(this).closest('.form__item').addClass('form__item--error');
                // disable continue button
                updateContinueBtn(false)
            }
        });

        $('#password').on('keyup input', function (e) {
            if ( $('.form__submit.existing').length ) {
                return;
            }
            let inputInfo = $(this).closest('.form__item').find('.input__info');
            if ($(this).val().length < 6) {
                inputInfo.text('Please create a password with at least 6 characters');
                $(this).closest('.form__item').addClass('form__item--error');
                // disable continue button
                updateContinueBtn(false)
            }
            else {
                inputInfo.text('');
                $(this).closest('.form__item').removeClass('form__item--error');
                // enable continue button
                updateContinueBtn(true)
            }
        });

        $('.fa-eye').on('click', function (e) {

            e.preventDefault();

            let $input = $(this).closest('.input__wrapper').find('input');

            if ( $input.attr('type') === 'password') {
                $input.attr('type', 'text');
                $(this).addClass('fa-eye-slash');
            }
            else {
                $input.attr('type', 'password');
                $(this).removeClass('fa-eye-slash');
            }

        });

    }

    const init = () => {
        cart3Store = store.getStore()
        events()
    }

    return {
        init: init
    }

})()

export default CartApp