import $ from 'jquery'
import {updateContinueBtn, PREVIEW_URL, getPreviewData, getDeliverySimpleValidation, isMobileNumber} from "../global"
import Actions from '../actions'
import updateTotals from '../components/Cart'
import Address from '../components/Address'
import {getSelector, selectors} from "../global/index";

/**
 * This Container is responsible for Delivery step in checkout
 *
 */

const DeliveryApp = (() => {



    const shippingOption = '.coredna-deliveryOptions input'

    const getSelectedRateId = (rateIdState, newRates) => {
        let previous = rateIdState.previous
        let newIDs = Object.keys(newRates)
        if(newIDs.length) {
            return newIDs.indexOf(String(previous)) !== -1 ? previous : newIDs[0]
        }
        else {
            return previous
        }
    }

    const fetchDeliveryRate = (deliveryRateId) => {

        let data = getPreviewData()
        data.delivery = deliveryRateId ? deliveryRateId : 0

        Actions.fetchPreview(function (error, response) {
            if (error) {
                // TODO: display errors
                return false
            }
            updateTotals(response)
        }, data)

    }

    const fetchDeliveryOptions = (deliveryRateId) => {

        let data = getPreviewData()

        if(deliveryRateId) {
            data.delivery = deliveryRateId
        }
        Actions.fetchPreview(function (error, response) {
            if(error) {
                // TODO: display errors
                return false
            }

        }, data)

    }

    const initiateFetchIfRequired = () => {
        if(getDeliverySimpleValidation() === true){
            setDeliveryValidation()
            fetchDeliveryOptions();
        }
        else{
            Actions.refreshDeliveryValidation(false)
            Actions.refreshSubmit(false, false)
        }
    }

    const setDeliveryValidation = () => {
        if(getDeliverySimpleValidation() === true && isMobileNumber($('#delivery_phone').val())) {
            Actions.refreshDeliveryValidation(true)
        }
        else {
            Actions.refreshDeliveryValidation(false)
        }
    }

    const events = () => {

        $('body').on('change', shippingOption, function (event) {

            fetchDeliveryRate(event.currentTarget.value)

        });

        $('#delivery_phone').on('input', function (e) {

            if (!isMobileNumber($(this).val())) {
                $(this).closest('.form__item').addClass('form__item--error');
            }
            else {
                $(this).closest('.form__item').removeClass('form__item--error');
            }
            setDeliveryValidation()

        });

        // User has completed typing street address. Now get shipping options if the postcode is filled
        $('#delivery_street_address').on('focusout', function(){
            initiateFetchIfRequired()
        });

        $('#delivery_postcode').on('change', function() {
            initiateFetchIfRequired()
        });

        $('#delivery_city').on('change', function(){
            initiateFetchIfRequired()
        });
        $('#delivery_select').on('change', function(){
            initiateFetchIfRequired()
        });


    }

    const init = () => {
        //fetchDeliveryOptions()
        initiateFetchIfRequired()
        events()
        Address.init()
        setTimeout(function () {
            Address.init()
        }, 3000)
    }

    return {
        init: init,
        fetchDeliveryRate,
        getSelectedRateId
    }

})()

export default DeliveryApp
