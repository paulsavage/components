import $ from 'jquery'
import CONSTANTS from '../global/constants'
import {getDeliverySimpleValidation} from "../global"
import store from '../reducers'
import cart from "../reducers/cart"
import Delivery from '../components/Delivery'
import DeliveryApp from '../containers/Delivery'
import Actions from '../actions'
import {getSelector, selectors} from "../global/index";

/**
 * This is where we listen to changes in Redux state.
 * If you need additional logic on state change, you can call your new functions from here. Look at the comment below.
 */

const Listener = (() => {

    let cart3Store;

    let previousState = {}

    const simpleObjCompare = (object1, object2) => {
        return JSON.stringify(object1) === JSON.stringify(object2)
    }

    const startListening = () => {
        cart3Store.subscribe(() => {


            let currentState = cart3Store.getState()

            let cart = currentState.cart

            if(currentState.status.deliveryLoading === false) {
                // update RateId
                Delivery.refreshRateIdInput(cart ? cart.rateId.current : null)
            }

            if(getDeliverySimpleValidation() === false) {
                Delivery.refreshDeliveryOptions(false, null)
            }
            else if(!previousState.cart) {
                Delivery.refreshDeliveryOptions(currentState.status.isFetching, null)
            }
            else if(currentState.status.isFetching && currentState.status.deliveryLoading) {
                Delivery.refreshDeliveryOptions(true, null)
            }
            //else if(currentState.status.fetchDelivery) {

            //}
            else if (currentState.status.isFetching === false && cart.response.shippingRates){
                let rateId = DeliveryApp.getSelectedRateId(cart.rateId, cart.response.shippingRates)
                //let rateId = cart.rateId.previous
                Delivery.refreshDeliveryOptions(false, cart.response.shippingRates, rateId)


                if(cart.response.zbs.error) {
                    let message = 'We are unable to deliver to the selected address. Please provide a different Delivery Address.'
                    Delivery.refreshDeliveryOptions(false, null, null, message)
                }
                else if(!cart.rateId.current) {

                    DeliveryApp.fetchDeliveryRate(rateId)
                }
            }

            // Refresh Submit Button
            let submit = currentState.status.submit

            let disableButton = !submit.enabled ||
                currentState.status.isFetching ||
                (cart.response.zbs && cart.response.zbs.error) ||
                currentState.status.deliveryValidation === false
            getSelector(selectors.submit).prop('disabled', disableButton)
            if(submit.loading === true) {
                getSelector(selectors.submit).addClass('loading')
            }
            else {
                getSelector(selectors.submit).removeClass('loading')
            }


            /**
             * Here you may include your own functions that will be called with new and previous state
             * Example is below:
             */
            // MY_AWESOME_FUNCTION(currentState, previousState)


            // DO NOT WRITE ANY CODE AFTER THIS LINE
            // at the end, save state for next update
            previousState = Object.assign({}, cart3Store.getState())

        })
    }

    const init = () => {
        cart3Store = store.getStore()
        startListening()
    }

    return {
        init
    }

})()

export default Listener
