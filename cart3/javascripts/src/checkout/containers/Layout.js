import $ from 'jquery'
import Layout from '../components/Layout'
import store from '../reducers'
import CONSTANTS from '../global/constants'
import {updateClassName} from '../global'

const LayoutApp = (() => {

    let cart3Store;

    let $submitBtn = $('.form-submit')

    const events = () => {

        $('form[data-form=continue-form]').on('submit', function () {
            $submitBtn.prop('disabled', true)
            $submitBtn.addClass('loading')
        })

    }

    const init = () => {
        cart3Store = store.getStore()
        Layout.init()
        events()
    }

    return {
        init: init
    }

})()

export default LayoutApp
