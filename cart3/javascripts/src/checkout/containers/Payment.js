import $ from 'jquery'
import {PREVIEW_URL, getVoucherErrorMessage, updateClassName, getAmountWithCurrency} from "../global"
import Actions from '../actions'
import updateTotals from '../components/Cart'
import store from '../reducers'

const PaymentApp = (() => {

    let cart3Store;

    let $voucherErrors = $('#voucher-errors')

    const applyCoupon = (coupon) => {

        Actions.refreshCouponBtn(true)

        $voucherErrors.html('')

        $.ajax({
            dataType: "json",
            url: PREVIEW_URL + coupon,
            success: function(response) {
                if (response.error) {
                    let minValue = response.error_data ? response.error_data.minCartValue : null
                    let minCount = response.error_data ? response.error_data.minCartQty : null
                    let message = getVoucherErrorMessage(response.error_code, minValue, minCount)
                    $voucherErrors.attr('class', 'errors');
                    $voucherErrors.html('<p>'+ message +'</p>');
                }
                else if (coupon === '') {
                    $voucherErrors.attr('class', 'errors');
                    $voucherErrors.html('<p>Discount voucher cleared.</p>');
                }
                else if (response.discounts.totals.total > 0)  {
                    $voucherErrors.attr('class', 'success');
                    $voucherErrors.html('<p>'+ getAmountWithCurrency(response.discounts.totals.vouchers) +' discount applied.</p>');
                }
                updateTotals(response)
            },
            error: function (err) {
                $voucherErrors.html('<p>Something went wrong. Please try again.</p>');
                console.error('VOUCHER ERROR !!! ',err);
            }

        }).done(function(msg) {
            Actions.refreshCouponBtn(false)
        });

    }

    const cardDetection = {

        initialize: function() {
            this.elements();
            this.events();
        },

        elements: function() {
            this.root = $('#cc_number');
        },

        cardTest: function(cardNumber) {
            // visa
            let re = new RegExp("^4");
            if (cardNumber.match(re) !== null)
                return "Visa";

            // Mastercard
            re = new RegExp("^5[1-5]");
            if (cardNumber.match(re) !== null)
                return "Mastercard";

            // AMEX
            re = new RegExp("^3[47]");
            if (cardNumber.match(re) !== null)
                return "AMEX";

            // Discover
            re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
            if (cardNumber.match(re) !== null)
                return "Discover";

            // Diners
            re = new RegExp("^36");
            if (cardNumber.match(re) !== null)
                return "Diners";

            // Diners - Carte Blanche
            re = new RegExp("^30[0-5]");
            if (cardNumber.match(re) !== null)
                return "Diners - Carte Blanche";

            // JCB
            re = new RegExp("^35(2[89]|[3-8][0-9])");
            if (cardNumber.match(re) !== null)
                return "JCB";

            // Visa Electron
            re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
            if (cardNumber.match(re) !== null)
                return "Visa Electron";

            return "";
        },

        events: function() {
            let baseClasses = 'input__card input--dirty ';
            cardDetection.root.on('keyup', function(e) {
                let currentCard = cardDetection.cardTest($(this).val());

                if (currentCard !== '') {
                    cardDetection.root.attr('class', baseClasses);
                    cardDetection.root.addClass('card__'+currentCard.toLowerCase());
                } else {
                    cardDetection.root.addClass('no-bg');
                }
            });
        }
    }

    const creditCardNext = {

        initialize: function() {
            this.elements();
            this.events();
        },

        elements: function() {
            this.ccMonth = document.getElementById('cc_expires_month');
            this.ccYear = document.getElementById('cc_expires_year');
        },

        events: function() {
            $(creditCardNext.ccMonth).bind('keyup', function(e) {
                if (($(this).val().length +1) > 2) {
                    $(this).parent().next().find('input').focus();
                }
            });
            $(creditCardNext.ccYear).bind('keyup', function(e) {
                if (($(this).val().length +1) > 2) {
                    $(this).parent().next().find('input').focus();
                }
            });
        }
    }

    const formatCC = (input, format, sep) => {
        let output = "";
        let idx = 0;
        for (let i = 0; i < format.length && idx < input.length; i++) {
            output += input.substr(idx, format[i]);
            if (idx + format[i] < input.length) output += sep;
            idx += format[i];
        }

        output += input.substr(idx);

        return output;
    }

    const events = () => {
        $('#apply-coupon').on('click', function (event) {
            event.preventDefault();
            applyCoupon($('#voucher-code').val());
        });
        $('#voucher-code').on('keypress', function(e){
            if ( e.keyCode === 13 || e.which === 13 ) {
                e.preventDefault();
                applyCoupon($('#voucher-code').val());
                return false;
            }
        });
        $('#cc_number').on('keyup',function(){

            // remove all non-digits
            let cardNumber = $(this).val().replace(/\D/g, "");

            if (cardNumber.length > 0) {
                cardNumber = formatCC(cardNumber, [4, 4, 4], "-");
            }

            $(this).val(cardNumber);

        });
        $('input[name=paymentselect]').on('change', function (e) {

            $('#payment-input').val( $(this).data('optionid') );

        });
        $('#billing_phone').on('keyup', function (e) {

            if ( !isMobileNumber( $(this).val() ) ) {
                $(this).closest('.form__item').addClass('form__item--error');
                // disable continue button
                $('.form__submit').prop('disabled', true);
                $('.form__submit').addClass('disabled');
            }
            else {
                $(this).closest('.form__item').removeClass('form__item--error');
                // enable continue button
                $('.form__submit').prop('disabled', false);
                $('.form__submit').removeClass('disabled');

            }

        });
        // handler change event for same billing address checkbox at payment page
        $('#same-delivery').on('change', function(){
            if ( $(this).prop( "checked" )  ) {
                $('#different_billing').val('FALSE');
            }
            else {
                $('#different_billing').val('TRUE');
            }
        });

        cart3Store.subscribe(()=> {
            let state = cart3Store.getState()

            let element = document.querySelector('.coupon-submit')
            updateClassName({
                element: element,
                className: 'loading',
                condition: state.status.applyCoupon === true
            })
            $(element).prop('disabled', state.status.applyCoupon)
        })
        //$('.coupon-submit').on('click', () => {
        //})
    }

    const initPayment = () => {

        Actions.fetchPreview(function (error, response) {
            if (error) {
                // TODO: display errors
                return false
            }
            updateTotals(response)
            Actions.refreshDeliveryValidation(true)
        })

    }

    const init = () => {
        cart3Store = store.getStore()
        initPayment()
        events()
        creditCardNext.initialize()
        cardDetection.initialize()
    }

    return {
        init: init
    }

})()

export default PaymentApp
