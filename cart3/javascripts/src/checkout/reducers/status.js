import CONSTANTS from '../global/constants'

const defaultState = {
    isFetching: false,
    submit: {
        loading: false,
        enabled: false
    },
    applyCoupon: false,
    deliveryValidation: false,
    deliveryLoading: true,
    fetchDelivery: false
}

const status = (state = defaultState, action) => {
    switch (action.type) {
        case CONSTANTS.REFRESH_STATUS:
            return {
                ...state,
                isFetching: action.isFetching,
                submit: action.submit,
                applyCoupon: action.applyCoupon
            }
        case CONSTANTS.REFRESH_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching
            }
        case CONSTANTS.REFRESH_SUBMIT:
            return {
                ...state,
                submit: action.submit
            }
        case CONSTANTS.REFRESH_APPLY_COUPON:
            return {
                ...state,
                applyCoupon: action.applyCoupon
            }
        case CONSTANTS.REFRESH_DELIVERY_VALIDATION:
            return {
                ...state,
                deliveryValidation: action.deliveryValidation
            }
        case CONSTANTS.REFRESH_DELIVERY_LOADING:
            return {
                ...state,
                deliveryLoading: action.deliveryLoading
            }
        case CONSTANTS.REFRESH_FETCH_DELIVERY:
            return {
                ...state,
                fetchDelivery: action.fetchDelivery
            }
        default:
            return state
    }
}

export default status