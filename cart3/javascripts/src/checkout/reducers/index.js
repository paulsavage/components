import { createStore, applyMiddleware, combineReducers } from 'redux'
import { createLogger } from 'redux-logger'
//import thunk from 'redux-thunk'
import Actions from '../actions'
import Listener from '../containers/Listener'
import {enableLogger} from '../global'

import cart from './cart'
import status from './status'

const CartStore = (() => {

    let store = null

    const create = () => {
        const checkoutApp = combineReducers({
            cart,
            status
        })
        const middleware = []
        if(enableLogger) {
            middleware.push(createLogger())
        }
        store = createStore(
            checkoutApp,
            applyMiddleware(...middleware)
        )
        Actions.init()
        Listener.init()
    }

    const getStore = () => (store)

    const init = () => {
        if(store === null) { // allow one copy
            create()
        }
    }

    return {
        init,
        getStore
    }

})()

export default CartStore