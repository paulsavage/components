import CONSTANTS from '../global/constants'
import {shippingRateId} from "../global"

const defaultState = {
    response: {},
    rateId: {
        previous: shippingRateId,
        current: shippingRateId
    }
}

const cart = (state = defaultState, action) => {
    switch (action.type) {
        case CONSTANTS.REFRESH_CART:
            return {
                ...state,
                response: action.response
            }
        case CONSTANTS.REFRESH_RATE_ID:
            return {
                ...state,
                rateId: action.rateId
            }
        default:
            return state
    }
}

export default cart