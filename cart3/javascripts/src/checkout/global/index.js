import $ from 'jquery'

const regexEmail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
const regexNumber = /^[0-9-+ ]+$/;

export const isEmail = (email) => {
    return regexEmail.test(email);
}

export const isMobileNumber = (number) => {
    return regexNumber.test(number) && number.length > 9;
}

export const updateContinueBtn = (enableIt) => {
    const $submit = $('.form__submit')
    if(enableIt === true) {
        $submit.prop('disabled', false).removeClass('disabled');
    }
    else {
        $submit.prop('disabled', true).addClass('disabled');
    }
}

export const PREVIEW_URL = '/index.php?action=prodcatalogueorder&form_name=preview&type=json&voucher_promo_code='

export const getDeliveryAfterDiscount = function (shipping, discounts){
    if(discounts && discounts.shipping && shipping > discounts.shipping.totals.total){
        return getAmountWithCurrency(shipping - discounts.shipping.totals.total)
    }
    else {
        return 'Free'
    }
}

export const CURRENCY = '$'

export const COUNTRY_CODE = 'US'

export const couponCode = $('#outer-content').data('coupon') || ''

//export const defaultRateId = $('#shippingRateId').val() || ''

const voucherErrors = {
    insufficient_amount: {
        message: `Minimum Total Cart for the voucher is #minCartValue#`,
        custom: `Sorry this voucher requires a order value of #CURRENCY##minCartValue# or more - please add more items & re-apply`
    },
    insufficient_quantity: {
        message: `Minimum Total Cart Quantity for the voucher is #minCartQty#`,
        custom: `Sorry this voucher requires a minimum of #minCartQty# items - please add more items & re-apply`
    },
    products_ineligible: {
        message: `No products associated with the voucher found in the cart`,
        custom: `Sorry your cart does not satisfy the voucher requirements.`
    },
    no_voucher: {
        message: `No Voucher Found`,
        custom: `Sorry that is not a valid voucher code.`
    },
    voucher_used: {
        message: `Voucher already being used`,
        custom: 'This voucher is already applied to the order.'
    },
    voucher_depleted: {
        message: `No Remaining Value available in the voucher`,
        custom: `Sorry this voucher has been used.`
    },
    voucher_expired: {
        message: `Voucher has expired`,
        custom: `Sorry that voucher has expired.`
    },
    voucher_invalid: {
        message: `Invalid Voucher`,
        custom: `Sorry that voucher is not valid.`
    }
}

export const getVoucherErrorMessage = (errorCode, minCartValue, minCartQty) => {
    let data = voucherErrors[errorCode]
    if(data) {
        let message = data.custom ? data.custom : data.message
        if(minCartValue) {
            message = message.replace(/#minCartValue#/, minCartValue)
            message = message.replace(/#CURRENCY#/, CURRENCY)
        }
        if(minCartQty) {
            message = message.replace(/#minCartQty#/, minCartQty)
        }
        return message
    }
    else {
        return 'Sorry, applied voucher code does not exist.'
    }
}

export const getPreviewData = () => {
    return {
        del_countryid: $('#delivery_countryId').val(),
        del_address : $('#delivery_street_address').val(),
        del_suburb: $('#delivery_city').val(),
        del_city : $('#delivery_city').val(),
        del_state: $('#delivery_select').val(),
        del_postcode : $('#delivery_postcode').val()
    }
}

export const getDeliverySimpleValidation = () => {
    let status = true

    let data = getPreviewData()
    for(let key in data) {
        if(!data[key]){
            status = false
        }
    }
    return status
}

export const updateClassName = (rule) => {
    let $elem = $(rule.element)
    if(rule.condition === true) {
        $elem.addClass(rule.className)
    }
    else {
        $elem.removeClass(rule.className)
    }
}

export const getAmountWithCurrency = (amount) => {
    return CURRENCY + Number(amount).toFixed(2)
}

const selectorPrefix = '.coredna-'

export const selectors = {
    count: 'count',
    delivery: 'delivery',
    discount: 'discount',
    subtotal: 'subtotal',
    tax: 'tax',
    total: 'total',
    deliveryOptions: 'deliveryOptions',
    rateId: 'rateId',
    submit: 'submit'
}

export const getSelector = (type) => {
    return $(selectorPrefix + selectors[type])
}

export const shippingRateId = window.shippingRateId || null

export const enableLogger = window.location.href.indexOf('corewebdna') >= 0


