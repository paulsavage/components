import $ from 'jquery'
import {couponCode, PREVIEW_URL} from '../global'
import CONSTANTS from '../global/constants'
import store from '../reducers'

const Actions = (() => {

    let cart3Store;

    const refreshCouponBtn = (loading) => {
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_APPLY_COUPON,
            applyCoupon: loading
        })
    }

    const refreshFetching = (loading) => {
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_IS_FETCHING,
            isFetching: loading
        })
    }

    const refreshSubmit = (enabled, loading) => {
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_SUBMIT,
            submit: {
                loading,
                enabled
            }
        })
    }

    const refreshCart = (response) => {
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_CART,
            response: response
        })
    }

    const refreshRateId = (previous, current) => {
        let previousId = previous ? previous : cart3Store.getState().cart.rateId.previous
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_RATE_ID,
            rateId: {
                previous: previousId,
                current: current
            }
        })
    }

    const refreshDeliveryValidation = (status) => {
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_DELIVERY_VALIDATION,
            deliveryValidation: status
        })
    }

    const refreshDeliveryLoading = (loading) => {
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_DELIVERY_LOADING,
            deliveryLoading: loading
        })
    }

    const refreshFetchDelivery = (loading) => {
        cart3Store.dispatch({
            type: CONSTANTS.REFRESH_FETCH_DELIVERY,
            fetchDelivery: loading
        })
    }

    // COREDNA

    const fetchPreview = (callback, data, coupon) => {
        let couponString = coupon ? coupon : couponCode
        refreshFetching(true)
        if(data) {
            let newRateId = data.delivery ? data.delivery : null
            refreshRateId(newRateId, newRateId)
            if(newRateId === null) {
                refreshDeliveryLoading(true)
            }
            else {
                refreshSubmit(false, true)
            }
        }
        else {
            refreshSubmit(false, true)
        }
        $.ajax({
            dataType: "json",
            url: PREVIEW_URL + couponString,
            data: data || {},
            success: function(response) {
                callback(null, response)
                refreshCart(response)
            },
            error: function(err) {
                callback(err)
                console.error('FETCH PREVIEW FAILED !!! ', err);
            }
        }).done(function (msg) {
            refreshFetching(false)
            refreshDeliveryLoading(false)
            refreshSubmit(true, false)
        });
    }


    const init = () => {
        cart3Store = store.getStore()
    }

    return {
        init,
        fetchPreview,
        refreshCouponBtn,
        refreshDeliveryValidation,
        refreshSubmit
    }

})()

export default Actions
