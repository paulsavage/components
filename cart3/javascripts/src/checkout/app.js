import store from './reducers'
import Cart from './containers/Cart'
import Delivery from './containers/Delivery'
import Payment from './containers/Payment'
import Layout from './containers/Layout'
import Guest from './components/Guest'
import Modal from './components/Modal'
import Form from './components/Form'

// Store init must be the first thing on page load
store.init()

// Some UI components
Layout.init()
Guest.init()
Modal.init()
Form.init()


// Initialise Containers
if(document.querySelectorAll('body.cart').length > 0) {
    Cart.init()
}

if(document.querySelectorAll('body.order-delivery').length > 0) {
    Delivery.init()
}

if(document.querySelectorAll('body.order-payment').length > 0) {
    Payment.init()
}

/***
 * You may call any additional functions here.
 */
