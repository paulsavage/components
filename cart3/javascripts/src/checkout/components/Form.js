import $ from 'jquery'

const Form = {

    events: function () {
        $('.form__item input, .form__item textarea').on('keydown keyup focus blur', function() {
            if( $(this).val() ) {
                $(this).addClass('input--dirty');

                // show the suburb, state and postcode fields container when street address has a value
                if ( $(this).attr('id')  && $(this).attr('id') == 'delivery_street_address' ) {
                    $('#delivery-fields').addClass('visible');
                }

            } else{
                $(this).removeClass('input--dirty');

                // hide the suburb, state and postcode fields container when street address is empty
                if ( $(this).attr('id') && $(this).attr('id') == 'delivery_street_address' ) {
                    $('#delivery-fields').removeClass('visible');
                }
            }

        });
    },

    init: function () {
        this.events()
    }

}

export default Form