import $ from 'jquery'
import {CURRENCY, getSelector, selectors} from "../global/index"
import Layout from './Layout'

const Delivery = {

    refreshDeliveryOptions: (loading, options, selectedRateId, message) => {
        let html = ''
        if(loading) {
            html = `<div style="padding:48px 32px; text-align: center;">
                            ${Layout.renderLoader()}
                        </div>`

        }
        else if(options) {
            let shipping = Object.keys(options).map(ii => options[ii])
            html = shipping.map((item) => {
                let checked = (String(item.deliveryId) === String(selectedRateId)) ? 'checked' : ''
                return `<div class="radio__copy">
                <input class="radio" value="${item.deliveryId}" data-deliveryid="${item.deliveryId}" data-price="${item.price}" type="radio" id="option--${item.deliveryId}" name="shippingradio" ${checked} />
                <label  class="radio__label" for="option--${item.deliveryId}"> 
                    <i class="fa"></i> 
                    ${CURRENCY}${item.price} - ${item.name} 
                </label>
            </div>`
            }).join(' ')

        }
        else {
            let msg = message ? message : 'Please complete Delivery Contact Information.'
            html = `<p style="padding: 32px;">
                            ${msg} 
                        </p>`

        }
        getSelector(selectors.deliveryOptions).html(html)
    },

    refreshRateIdInput: (rateId) => {
        let value = rateId ? rateId : ''
        let input = getSelector(selectors.rateId)
        input.val(value)

    }

}

export default Delivery