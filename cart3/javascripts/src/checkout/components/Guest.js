import $ from 'jquery'

const Guest = {

    events: function () {
        $('#guest-checkout').on('click', function (event) {

            if ( $('#upsell-content').length && $(this).hasClass('disabled') ) {
                event.preventDefault();
                // show a message to the user
                let html = $('#upsell-content').html();
                $('#cart-info-msg').html(html);
                $('.cta__wrapper').hide();
                $('#modal-close').show();
                $('body').addClass('show-modal');
            }
            $(this).removeClass('disabled');

        });
    },

    init: function() {
        this.events()
    }

}

export default Guest