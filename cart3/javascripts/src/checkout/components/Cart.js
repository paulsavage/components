import $ from 'jquery'
import {getSelector, selectors, getAmountWithCurrency, getDeliveryAfterDiscount} from "../global";

const updateTotals = (response) => {
    getSelector(selectors.total).html(getAmountWithCurrency(response.total))
    getSelector(selectors.tax).html(getAmountWithCurrency(response.tax))
    getSelector(selectors.discount).html(getAmountWithCurrency(response.discounts.products.totals.total))
    getSelector(selectors.delivery).html(getDeliveryAfterDiscount(response.shipping, response.discounts))
}

export default updateTotals
