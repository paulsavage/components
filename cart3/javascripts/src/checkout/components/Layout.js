import $ from 'jquery'

const Layout = {

    stickyOnScroll: function() {

        let headerHeight = $('header').outerHeight(true),
            mainHeight = $('main').outerHeight(true),
            asideHeight = $('.content--aside').height(),
            footerHeight = $('.footer').outerHeight(true);

        let sidebar = $('.content--aside');

        let offset = $('.content__cart__info').outerHeight(true) || 0;

        if ( !isMobile && ($(window).scrollTop() > (headerHeight + offset)) && ($('.content--main').height() > $('.content--aside').height()) ) {

            if ($(window).scrollTop() > (headerHeight + mainHeight - asideHeight - footerHeight)) {
                $('body').removeClass('sticky--aside');
                $('body').addClass('sticky--aside--bottom');
                sidebar.css('margin-right', 0);
            }
            else {
                $('body').addClass('sticky--aside');
                $('body').removeClass('sticky--aside--bottom');
                $('.content--aside').width( $('#outer-content .content__inner').width() * 0.3157 );
                sidebar.css('margin-right', rightMargin);
            }

        } else {
            $('body').removeClass('sticky--aside');
            sidebar.css('margin-right', 0);
        }

    },

    events: function () {
        const self = this
        $(window).scroll(self.stickyOnScroll);
        self.stickyOnScroll();

        // window resize handler for sticky sidebar
        $(window).resize(function(e){

            window.rightMargin = $('#outer-content .content__inner').offset().left;

            window.isMobile = $(window).width() < 768;

            if (isMobile) {

                $('.content--aside').width('auto');
                $('body').removeClass('sticky--aside');
                $('body').removeClass('sticky--aside--bottom');
            }
            else {
                $('.content--aside').width( $('#outer-content .content__inner').width() * 0.3157 );
            }

            self.stickyOnScroll();

        });

        $(window).scroll(function() {
            if (window.pageYOffset > 110 && isMobile) {
                $('body').addClass('navigation--sticky');
            } else {
                $('body').removeClass('navigation--sticky');
            }
        });

        $('.steps__item.disabled a').on('click', function (e) {

            e.preventDefault();

        });
    },

    renderLoader: function () {
        return `<div class="loader">
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
            </div>`
    },

    init: function () {
        window.rightMargin = $('#outer-content .content__inner').offset().left;
        window.isMobile = $(window).width() < 768;
        this.events()
    }

}

export default Layout