import $ from 'jquery'
import {COUNTRY_CODE} from '../global'

const Address = (() => {

    let deliveryAutocomplete = null

    const deliveryComponentForm = {
        street_number: [{'target':'delivery_street_address','value':'short_name'}],
        route: [{'target':'delivery_street_address','value':'long_name'}],
        locality: [{'target':'delivery_city','value':'long_name'}],
        administrative_area_level_1: [{'target':'delivery_state','value':'short_name'}],
        postal_code: [{'target':'delivery_postcode','value':'short_name'}],
        country: [{'target':'delivery_countryid','value':'short_name'}]
    }

    const fillInAddressDelivery = () => {
        let delivery = deliveryAutocomplete.getPlace();

        /**
         * Use console log to see the data received in delivery variable above
         * We simply loop through the address_components and fill in HTML elements
         */

        let hasStreetAdd = false;
        let streetNum = '';
        let subpremise = ''; // subpremise is Unit number

        for (let i = 0; i < delivery.address_components.length; i++) {
            let addressType = delivery.address_components[i].types[0];
            if ( addressType == 'subpremise' ) {
                subpremise = delivery.address_components[i].short_name;
                streetNum = subpremise + '/' + streetNum;
            }
            if (deliveryComponentForm[addressType]) {


                let val = delivery.address_components[i][deliveryComponentForm[addressType][0]['value']];
                let target = deliveryComponentForm[addressType][0]['target'];

                if (addressType == 'street_number') {
                    streetNum = streetNum + val;
                }
                if (addressType == 'street_number' || addressType=='route') {
                    hasStreetAdd = true;
                }

                if (addressType == 'administrative_area_level_1') {
                    $('#delivery_select option:selected').removeAttr('selected');
                    $('#delivery_select').val(val);
                } else if (addressType == 'route') {
                    let street_number = $('input[name="'+target+'"]');
                    if (streetNum != '') {
                        street_number.val(streetNum + ' ' + val);
                    } else {
                        street_number.val(val);
                    }
                } else {
                    $('input[name="'+target+'"]').val(val).trigger('change');
                    $('input[name="'+target+'"]').addClass('input--dirty')
                }
            }
        }

        if (!hasStreetAdd) {
            let street_number = $("#"+deliveryComponentForm['street_number'][0]['target']);
            street_number.val(street_number.val().substr(0, street_number.val().indexOf(",")));
        }
    }

    const events = () => {
        deliveryAutocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('delivery_street_address')),
            {
                types: ['geocode'],
                componentRestrictions: {country: COUNTRY_CODE}
            });
        deliveryAutocomplete.addListener('place_changed', fillInAddressDelivery);
    }

    const init = () => {
        if ($('#delivery_street_address').length > 0 && deliveryAutocomplete === null) {
            events()
        }
    }

    return {
        init: init
    }

})()

export default Address