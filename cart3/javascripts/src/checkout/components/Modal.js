import $ from 'jquery'

const Modal = {

    closeModalWindow: function() {
        $('body').removeClass('show-modal');
    },

    events: function () {

        const self = this
        /**
         * Close button functionality for modal window
         */
        $('.cta--modal-close, #modal-close').on('click', function(e) {
            e.preventDefault();
            self.closeModalWindow();
        });
        $(document).keyup(function (e) {

            // close modal window on ESC key press
            if ( e.keyCode == 27 && $('body').hasClass('show-modal') ) {
                self.closeModalWindow();
            }

        });
        $('body').on('click', function (e) {

            // close modal window when clicked on blackish background
            let targetClass = e.target.className;
            if ( $(this).hasClass('show-modal') && targetClass.indexOf('section--modal') >= 0 ) {
                self.closeModalWindow();
            }

        });
    },

    init: function () {
        this.events()
    }
}

export default Modal