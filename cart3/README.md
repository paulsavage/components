# Standard Cart3 Implementation 

It is tested with Coredna Release 2018.11. A sample website using this Component is Stanley PMI on branch name `aww-cart3`. 

## First things first: What is it?

* It is a standalone front-end implementation for entire checkout process using Cart3. 
* It is NOT a snippet for adding a product to cart. 
 

## TL; DR

0. There is no TL; DR for this component. Please read the whole documentation.  
0. It is strongly recommended to understand the contents of this documentation before you begin to modify any code. 
0. It is advised to setup the DXP configuration for Cart3 before you begin front-end integration of Cart3.
0. Fastest way to integrate Cart3 is by following documentation below and using Westwood development server.
0. This component uses design patterns which require modern Front-end skills in order to modify any code. Please refer to the section **Tech Involved**. If you are unsure about certain functionality after reading the documentation, please consult a Coredna Developer.             

## Tech Involved

* ES6 JavaScript
* Smarty
* SCSS 
* jQuery
* Redux
* Webpack and Babel 

## Limitations

* Delivery rate functionality relies on the selected rateId in AJAX response which is not released yet.
* AJAX actions are not supported on Bundles in cart.
* Please read the section **What's there for future improvement** to determine if any missing feature will impact your project. 


## What is done so far

* Complete smarty build for checkout steps
* AJAX update on product quantity 
* State management of Delivery Rates using Redux 
* State management of Submit button status using Redux
* AJAX update for Totals
* AJAX update for Coupon code   
* Overall error handling 

## What's there for future improvement 

* Implement Bundles   
* Add state management to Product updates using AJAX 
* Improve jQuery implementation for various DOM updates
* Move some of the event handlers to Containers    
* Add even more state management with Redux 
* Convert Preview AJAX request to use generic `content_type` input for JSON  
* Convert Cart Login form to use AJAX
* Improve Continue submit button height for smaller screen 
* Add Front-end validation for Delivery Names 
* Convert Country and States to use a settings from DXP
* Convert Shop URL to use a settings from DXP 
* Find a more generic way to show tax when it is included or excluded from subtotal 
* Configure webpack to handle rest of the website's scripts / styles  
* Improve Smarty functions to show totals during initial page render from server
* Plug eCommerce tracking scripts   


## Steps Involved for Cart3 Integration

Here is the recommended step by step process to integrate Cart3 in your project:                         
 
0. Configure DXP to use Cart3
0. Prepare Metadata and Banner content for Cart3
0. Configure Zone Based Shipping in DXP - Not covered in this documentation 
0. Setup some payment options in DXP - Not covered in this documentation
0. Configure Hooks in DXP for Order emails - Not covered in this documentation
0. Copy Cart3 Component files to your front-end project 
0. Change branding or colours to match your design  
0. Check build system, do `npm install` and recompile Cart3 source code for JavaScript and Stylesheets
0. Add products to cart and go through various test cases manually 


## DXP Configuration for Cart3 

Cart3 requires following Centre Options to be added to the Centre Configuration in DXP:         

0. cart3
0. new_order_process


### Content requirements for Cart3 

Cart3 uses some content from Centre Metadata in DXP. Following Meta names are supported:

0. `Cart3 Logo` - This is the main image for logo on the checkout pages. If this meta is not found, template will display Coredna logo.
0. `Cart3 Payment Logos` - You can replace VISA MASTERCARD logos in the footer using this meta. 
0. `Cart3 Guest Upsell` - Here you can add marketing text to convince people to sign-up for an account. 
0. `Cart3 Chat Link` - You can specify a custom URL for a chat page or help.  
0. `Cart3 Secure Logo` - Use this if you want to show a Comodo Secure logo for example. 
0. `Cart3 Confirmation Message` - This is the confirmation text which is displayed when an order is completed. If this meta is not found, template will display default confirmation message. 

### Marketing Banners for Cart3 

Cart3 component supports marketing banners on each step of the checkout process. The banner category must be `Cart3 Banners` for this feature to work. Following Banner titles are supported:        
     
0. Cart
0. Delivery 
0. Payment
0. Confirmation 
      
Each banner corresponds to a step during the checkout process. Marketing text can be added to the Banner content in DXP.

         
## Copying Cart3 Component to your project 

For most practical scenarios, you will already have existing files in your project. Follow along to copy Cart3 code in the relevant directories. 

The complete directory structure for Cart3 component can be seen here: 

     
```bash
├── javascripts
│   └── src
│       ├── checkout
│       │   ├── actions
│       │   ├── app.js
│       │   ├── components
│       │   ├── containers
│       │   ├── global
│       │   └── reducers
│       └── modules
├── modules
│   ├── advertisement
│   │   └── templates
│   │       └── _blank.html
│   ├── centres
│   │   └── templates
│   │       └── _blank.html
│   ├── prodcataloguecart
│   │   └── templates
│   │       └── index.html
│   └── prodcatalogueorder
│       └── templates
│           ├── complete.html
│           ├── customer_details.html
│           ├── index.html
│           └── payment.html
├── package.json
├── public
│   ├── fonts
│   │   ├── checkout
│   │   ├── FontAwesome.otf
│   │   ├── fontawesome-webfont.eot
│   │   ├── fontawesome-webfont.svg
│   │   ├── fontawesome-webfont.ttf
│   │   ├── fontawesome-webfont.woff
│   │   └── fontawesome-webfont.woff2
│   ├── images
│   │   └── checkout
│   ├── scripts
│   │   └── checkout
│   │       └── checkout.js
│   └── styles
│       └── checkout
│           └── main.css
├── README.md
├── stylesheets
│   └── src
│       └── checkout
│           ├── checkout.scss
│           ├── components
│           ├── font-awesome.scss
│           ├── operon
│           └── _settings.scss
├── templates
│   └── layout--order.html
└── webpack.config.js

```


#### javascripts
 
Copy or replace `src/checkout` folder.    

#### modules 

* Copy or replace all files from `prodcataloguecart` and `prodcatalogueorder` module folder.     
* Make sure `_blank.html` file exists in your project templates for `advertisement` and `centres` module folders.         

#### webpack 

Copy over `webpack.config.js` and `package.json` to the root of your project. A couple of things to consider are below:     
    
0. If `package.json` exists in your project, please update the contents of scripts, dependencies, devDependencies and browserslist keys.     
0. If `webpack.config.js` exists in your project,  please add the new scssConfig and jsConfig configuration manually.       

#### public

It contains static assets, locally hosted fonts and compiled scripts for Cart3. You can copy the entire folder as it is. If some sub folders exist in your project, you can merge contents. 

#### stylesheets

Copy or replace `src/checkout` folder

#### templates 

Copy or replace `layout--order.html`          



## Nitty-Gritty of Cart3 Implementation 

### Good Ol' Smarty 

#### Base Template 

Every page or step in the checkout process must inherit `layout--order.html` template file. This is where we define base layout for entire checkout process including header, footer, navigation and marketing banners. You may also include additional scripts in this template if required.          

#### Cart Module 

It includes initial cart summary screen and the Cart Login form. Cart Summary supports product quantity update using AJAX. Login form is a traditional form with following features:                     
        
* Registration of a new user 
* Login for an existing user 
* Guest Checkout 
* Guest Checkout marketing upsell
* Front-end validation on Email and Password fields        

#### Order Module 

This is where most of the order processing happens. There are three main HTML templates: 

0. **customer_details.html** - This template contains a form to collect Delivery Contact Information from customer. 
0. **payment.html** - This template contains a form to collect Billing Contact Information. It also displays input fields for coupon code and credit card payment.    
0. **complete.html** - This template handles confirmation message upon order completion. It can also display an error message if we encountered a critical error while processing the order upon payment.

The smarty code in above mentioned templates is quite straightforward. You should be able to see easily what is happening in each template. 

### Stylesheets 

The base source folder for styles is `stylesheets/src/checkout`. Styles in Cart3 Component are built using:     

0. SCSS (of course)         
0. Operon Grid and Mixin library         
0. Font Awesome 4.7      


#### Changing Brand Settings

0. `_settings.scss` contains SASS variables for colours, fonts, transition and border radius. You can change these variables as required.    
0. `components` folder has UI styles for entire checkout process. Here you will be able to change the styles for forms, inputs, cards, buttons or any content element.   
  

### Public Assets 

0. Cart3 layout uses certain icons and logos which are stored in `public/images/checkout` folder.
0. Cart3 layout uses Roboto, Lato and Font Awesome fonts. All those fonts files are stored in `fonts` folder. 
0. `scripts` and `styles` folders contain webpack generated Stylesheet and JavaScript files for production use.   

### JavaScript - ES6 

The base source folder for JavaScript is `javascripts/src/checkout`. This is where most of the business logic is implemented in front-end. Source code is divided into following parts:     

* **app.js** - It is application entry point. It kick-starts Redux Store and relevant Container module depending upon current checkout step. 
* **actions** - It contains Action creators or Async request functions.  
* **components** - It contains jQuery code responsible for updating views.  
* **containers** - It contains ES6 modules for event handlers, making use of Action creators and listener for updates upon change in Redux state.  
* **global** - Anything you want to share between Components, Containers and Action creators. 
* **reducers** - It contains logic for creating a new Redux store, getStore function and combining different reducers to generate a single State object.   

Here is some more detail about each of the above: 

#### actions

The most important function here is `fetchPreview` which is being used to get new Delivery rates. `fetchPreview` actually uses many different action creators to handle an Async request to Coredna Preview function. These Action creators can dispatch new changes in the state which eventually trigger UI updates. `fetchPreview` supports a callback function as well.    

#### components 

The sole purpose of Components is to update views (UI). You may define some basic event handlers here too as long as these event handlers do not need to invoke changes in the global Redux state.    

#### containers 

This is the most important part of the Cart3 JavaScript logic. Containers modules should handle event handlers requiring Async actions and state management with Redux. Once an action or state change has been completed, a relevant Container module is responsible for invoking a Component to update UI. This allows us to separate our business logic from the UI stuff. More details on Containers will appear below. 

#### global 

There is nothing fancy here in `index.js`. Although you may want to change the Currency code and symbol from this file for your project. `constants.js` contains all Action types being used in Cart3. 

#### reducers

Reducers work with Action Creators. A reducer can combine previous state with new action to generate an updated Redux State.                    
   
`index.js` exports an ES6 module that can be imported in any JS file. It exposes a function called `getStore()` that will provide a reference to global instance of Redux Store. Currently two types of reducers are being used in Cart3.                        

0. `cart.js` stores Preview AJAX response and selected rateId data.     
0. `status.js` stores async and UI elements' state.            
 




  




















