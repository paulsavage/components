var webpack = require('webpack');
var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer')

const extractStyles = new ExtractTextPlugin({
    filename: "[name].css",
    disable: false
});


var stylesSource = [
    path.resolve(__dirname, 'stylesheets/src/all/all.scss')
]

const getAllStylesSources = () => {
    return stylesSource
}

var scssConfig = {
    entry: {
        main: getAllStylesSources()
    },
    output: {
        path: __dirname + '/public/styles/checkout/',
        filename: '[name].css'
    },
    module : {
        rules: [{
            test: /\.scss$/,
            use: extractStyles.extract({
                use: [{
                    loader: "css-loader",
                    options: {
                        modules: true,
                        importLoaders: true,
                        localIdentName: "[local]"
                    }
                },{
                    loader: 'postcss-loader',
                    options: {
                        ident: 'postcss',
                        plugins: () => [ autoprefixer() ]
                    }
                },{
                    loader: "sass-loader"
                }],
                // use style-loader in development
                fallback: "style-loader"
            })
        }]
    },
    plugins: [
        extractStyles
    ]
};


const JS_SRC = path.resolve(__dirname, 'javascripts/ecommerce');
const JS_DIST = path.resolve(__dirname, 'public/scripts/ecommerce');

const jsConfig = {
    entry: JS_SRC + '/app.js',
    output: {
        path: JS_DIST,
        filename: 'ecomm.js'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json']
    },
    module : {
        rules : [
            {
                test : /\.jsx?/,
                include : JS_SRC,
                loader : 'babel-loader'
            }
        ]
    }
};

module.exports = [
    //scssConfig,
    jsConfig
]
