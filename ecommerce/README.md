# E-commerce tracking 

This component has been tested with Coredna Release 2018.11

## What is this?

Almost all of e-commerce websites require analytics tracking for product impressions, product clicks, various cart actions and order steps. E-commerce tracking component is a standard implementation for tracking using JavaScript.

## Analytics Services 

* Google Enhanced E-commerce Tracking - Developer Guide is [here](https://developers.google.com/tag-manager/enhanced-ecommerce) 
* Facebook Pixel Tracking 

## Tech involved 

* ES6 JS
* Webpack

## Limitations

* Some functionality is dependent on a future Coredna release specified in TECH-4082
* Even though it is a standalone implementation, it relies on custom data based on business logic of a website within smarty templates. 

## What is done so far 

Following events or actions are being tracked at the moment: 

* Product Impressions - Google 
* Product Clicks - Google 
* Product Detail Impression - Google
* Product Variant Detail Impression - Google
* Add to Cart - Google  

## What's there for future

* Cart and Order process related Google tracking 
* Facebook Pixel tracking 

## How to use this component 

A practical implementation using this component can be seen at branch name `blackbox-tracking` of tivoliaudiocom repository. Read the developer docs above for detailed introduction. 

### Build system 

This component is using webpack to transpile ES6 code. If you are not using webpack, you can drop the `webpack.config.js` and `.babelrc` in your project root folder. Then create or update `package.json` from the example provided in this component. Webpack is configured to compile production JS file into `public/scripts/ecommerce/` folder. 

Once you are done with `npm install` and `npm run build`, you may include following line near the end of `body` tag in your project.

```
<script src="<{$smarty.session.centre_asset_host}>/public/scripts/ecommerce/ecomm.js"></script>
```

Alternatively, if you are using Gulp with Babel, you can add another task in gulpfile.js to compile E-commerce component JS.

### Code structure 
 
* **`app.js: `** It is application entry point. It assigns a global variable `COREDNA_ECOMM` to window. 
* **`catalogue.js: `** It contains functionality related to Product Impressions, Clicks or Add/Remove Product actions 
* **`cart.js: `** Cart related actions 
* **`order.js: `** Order steps tracking 
* **`global.js: `** Global constants 
* **`helpers.js: `** Reusable Helper functions 


### Product Impressions 

E-commerce tracking component requires you to create following html element with specified data attributes. For product listings, you can create one div element per product.  

```
<div class="ecommerce-list-item-data" style="display: none;"
     data-name="Digital Radio"
     data-id="99"
     data-price="55.99"
     data-category="Radios"
     data-variant="Black"
     data-list=""
     data-position="1"
></div>
```

Details about each data attribute are below: 

* `name: ` Product Name    
* `id: ` Product ID    
* `price: ` The final price for product or selected option     
* `category: ` Product Category Name    
* `variant: ` Product Variant Name   
* `list: ` You can specify custom strings here depending upon current state of the page. For example, When user has applied a faceted search filter, you may append the facet name to list. Only applicable to listings      
* `position: ` Product position within listing - Only applicable to listings     


### Product Clicks 

You may track product URL clicks within a product listing. A sample markup will look like this:  

```
<div data-ecomm-product="1" class="product_container">
    <a href="/shop/PRODUCT_URL" class="ecommerce-product-link">
        Product Name 
    </a>
    <div class="ecommerce-list-item-data" style="display: none;"
         data-name="Digital Radio"
         data-id="99"
         data-price="55.99"
         data-category="Radios"
         data-variant="Black"
         data-list=""
         data-position="1"
    ></div>
</div>
```
Things to remember are:      

0. The product container div has a `data-ecomm-product="1"` attribute        
0. The product URL has a class name `ecommerce-product-link`   
0. We are using same data attributes as product impressions              


### Related Products    

You may track impressions for the related products within a success modal for Add to cart action. Markup for related product looks like this:  
```
<div class="ecommerce-related-item-data" style="display: none;"
     data-name="Digital Radio"
     data-id="99"
     data-price="55.99"
     data-variant="Black"
     data-list="Related Products"
     data-position="1"
></div>
```

### Add to Cart

It works with a callback function. There is no requirement for HTML markup.     

### Catalogue Functions 

Catalogue object has following accessible functions in it: 

```
{
    catalogue: {
        getListData: function ($form, price) {},
        sendAddRemoveAction: function (action, eventType, productId, quantity) {},
        sendImpressions: function ($data) {},
        sendProductDetailImpression: function (eventType, productId) {},
        updateListItemData: function (variant, productData) {}
    }
}
```

#### getListData 

You may use this function to get the value for list data in your faceted search. It requires two arguments:      

0. `$form` - A jQuery object for Facets form     
0. price - Price value if a price filter is active. It should be empty otherwise            

#### sendAddRemoveAction 

You may use this function when a product is successfully added to cart or removed from cart. The required paramters are below:     

0. `action` - it can have 'add' or 'remove' value depending upon your action       
0. `eventType` - Event type as configured in GTM      
0. `productId` - Product ID      
0. `quantity` - Product quantity      

#### sendImpressions 

It works with your faceted search.    

0. `$data` - You can pass the HTML response for Products listing to this function     

#### sendProductDetailImpression  

It is used on a product detail page. It can be used both on page load or when user has selected a different variant option.      

0. `eventType` - Event type as configured in GTM      
0. `productId` - Product ID     

#### updateListItemData 

When a user has selected a different variant option on product page or product listing page, you may want to update E-commerce data attributes so that we always send up-to-date data for tracking. You can call this function with new variant data and product ID as specified below:      
 
0. `variant` - It should be the variant data received from Coredna (for example using show_variant_filters_v2)      
0. `productId` - Product ID       

**`Note: `** This function automatically sends a Product Variant Detail Impression for tracking.     

